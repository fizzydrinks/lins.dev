const i18nextConfig = {
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'pt-BR', 'tp', 'tp-SP'],
  },
};

module.exports = i18nextConfig;
