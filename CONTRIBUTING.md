# Contributing

Thanks for wanting to participate. You can
[send a merge request](https://gitlab.com/gabrielchiconi/lins.dev/merge_requests),
[file an issue](https://gitlab.com/gabrielchiconi/lins.dev/issues) or
[check out my huge to do list on Trello](https://trello.com/b/lx9Hi46a/linsdev).
